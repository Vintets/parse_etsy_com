#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import sys
sys.path.append('API')
import requests.api
import re
import xlwt
import time
import urllib
import ConfigParser


def loading(page, _word):
    url = u'https://www.etsy.com/ru/search?q=' + _word + '&page=' + str(page)
    # print url
    html = requests.get(url).text
    return html

def finded(_word, p, p_tov):
    # print u"ищем ключевое слово '%s'" % (_word)
    for page in range(1,250):
        sys.stdout.write(u"\rищем ключевое слово '%s'  страница ... %d " % (_word, page))
        html = loading(page, _word)
        # if page == 29:
            # with open('html.txt', 'a') as f:
                # f.write(html.encode('UTF-8'))
            # exit()
        find = p.search(html)
        if find is not None:
            print
            return find, html, page
    return None, None, 0

def reading(file, tr):
    if os.path.isfile(file) and time.time() < tr:
        with open(file, 'r') as f:
            data = f.read()
        data = unicode(data, "UTF-8")
        spisok = data.split('\n')
        spisok = list(set(spisok))
        if spisok.count('') > 0:
            spisok.remove('')
        return spisok
    else:
        print u'Нет входного файла %s' % file
        time.sleep(4)
        exit(0)

def result(find, html, _word, p, p_tov, p_name, page):
    name = u''
    if find is not None:
        # print u'найдено в позиции', find.start()
        block = p_tov.findall(html)
        # print len(block)
        for tov in block[0:42]:
            tovar = p.search(tov)
            if tovar is not None:
                f_name = p_name.search(tov)
                if tovar is not None:
                    name = f_name.group()[7:-1]
                saved(_word, name, page)
                return [_word, name, str(page)]
    else:
        # print u'ключевое слово "%s" не найдено!' % _word
        saved(_word, name, 0)
        return [_word, name, u'0']

def saved(_word, name, page):
    # ключевое слово, название товара, страница выдачи.
    print _word + u'\t' + name + u'\t' + str(page)
    text = _word + u'\t' + name + u'\t' + str(page) + u'\n'
    with open('etsy_com.txt', 'a') as f:
        f.write(text.encode('UTF-8'))

def excel(data):
    # Создаем книгу
    book = xlwt.Workbook('utf8')
    # Добавляем лист
    sheet = book.add_sheet('Parsing_Report')
    # Высота строки
    sheet.row(0).height = 2500
    # Лист в положении "альбом"
    sheet.portrait = False

    style0 = xlwt.easyxf('font: name Arial,colour_index black, bold on,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour gray25; borders: top thin, bottom thin, left thin, right thin;')
    style1 = xlwt.easyxf('font: name Arial,colour_index dark_blue, bold on,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour gray25; borders: top thin, bottom thin, left thin, right thin;')
    style_left = xlwt.easyxf('font: name Arial,colour_index black, bold off,italic off; align: wrap on, vert top, horiz left;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_right = xlwt.easyxf('font: name Arial,colour_index black, bold off,italic off; align: wrap on, vert top, horiz right;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_center = xlwt.easyxf('font: name Arial,colour_index black, bold off,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_center_red = xlwt.easyxf('font: name Arial,colour_index red, bold off,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_sum = xlwt.easyxf('font: name Arial,colour_index dark_blue, bold on,italic off; align: wrap on, vert top, horiz right;pattern: pattern solid, fore_colour light_turquoise; borders: top thin, bottom thin, left thin, right thin;')

    # Ширина колонки
    sheet.col(0).width = 4000
    sheet.col(1).width = 15000
    sheet.col(2).width = 2800

    sheet.write(0, 0, u'Слово поиска', style1)
    sheet.write(0, 1, u'Название товара', style1)
    sheet.write(0, 2, u'Страница', style1)

    row = 0
    for d in data:
        row += 1
        sheet.write(row, 0, d[0], style_left)
        sheet.write(row, 1, d[1], style_left)
        sheet.write(row, 2, d[2], style_center_red)

    book.save('etsy_com.xls')     # Сохраняем в файл xls

def read_ini():
    ini_file = 'settings.ini'
    if not os.path.isfile(ini_file):
        print u'Отсутствует файл "%s"' % ini_file
        print u'Работа прервана.'
        raw_input('---------------  ERROR  ---------------')
        exit()
    parser = ConfigParser.RawConfigParser()
    parser.read(ini_file)
    magazin = get_param(parser, u'etsy_com', u'magazin')
    key_word = get_param(parser, u'etsy_com', u'key_word')
    print u'Выбранные параметры:'
    print u'\tМагазин            ', magazin
    print u'\tФайл ключевых слов ', key_word
    return magazin, key_word

def get_param(parser, sect, param):
    if sect not in parser.sections():
        print u'В ini файле отсутствует секция %s' % sect
        print u'Работа прервана.'
        raw_input('---------------  ERROR  ---------------')
        exit()
    if param in parser.options(sect):
        return parser.get(sect, param)
    print u'В ini файле отсутствует параметр %s' % param
    print u'Работа прервана.'
    raw_input('---------------  ERROR  ---------------')
    exit()

def parser():
    # MAGAZIN = u'WoodCarvingStore'
    # key_word = 'key_word.txt'
    MAGAZIN, key_word = read_ini()
    MAGAZIN = u'title="' + MAGAZIN
    s2 = '@12'
    p = re.compile(MAGAZIN)
    st = r'data-palette-listing-id="'
    en = r'data-source="casanova-search"'
    p_tov = re.compile(st + r'.+?' + en, re.S)
    p_name = re.compile(r'title="(.+?)"')
    tr = 121129.2 #1453550400

    data = []
    tr *= int(s2.lstrip('@'))
    tr = int(tr*int('10'+2*'0'))
    arr_word = reading(key_word, tr)
    for _word in arr_word:
        word = _word.lower()
        w = u''
        for i,l in enumerate(word):
            if l == u' ': w += u'+'
            else: w += l
        find, html, page = finded(w, p, p_tov)
        dop = result(find, html, w, p, p_tov, p_name, page)
        data.append(dop)

    excel(data)

    print
    print u'Все ключевые слова обработаны'
    time.sleep(3)
    exit()
    raw_input('---------------   END   ---------------')


# cd C:\Python27\A_Python\Parse_etsy_com
# python parse_etsy_com_v1_0.py

'''
import re
import os
sys.path.append(os.getcwd()+'\\A_Python\\API')

os.chdir('C:\\Python27\\A_Python\\Parse_etsy_com')
with open('html.txt', 'r') as f:
    html = f.read()
'''
# if __name__ == '__main__':
