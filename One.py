#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import sys
sys.path.append('API')
import requests.api
import re
import xlwt
import time
import random
import urllib
import ConfigParser


def UserAgent():
    brow = random.randint(2, 3)
    if brow == 1:
        f = float(random.randint(30, 43))
        if f != 43.0:
            f += random.randint(0, 7) / 10.0
        ua = "Mozilla/5.0 (Windows NT 5.1; rv:" + str(f) + ") Gecko/20100101 Firefox/" + str(f)
    elif brow == 2:
        ch = [
                'Mozilla/5.0 (Windows NT 8.1) AppleWebKit/536.21 (KHTML, like Gecko) Chrome/32.0.2018.39 Safari/536.21',
                # 'Mozilla/5.0 (Windows NT 7.1; WOW64) AppleWebKit/536.12 (KHTML, like Gecko) Chrome/31.0.2043.42 Safari/536.12',
                # 'Mozilla/5.0 (Windows NT 7.0; Win64; x64) AppleWebKit/536.25 (KHTML, like Gecko) Chrome/37.0.2035.63 Safari/536.25',
                # 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.34 (KHTML, like Gecko) Chrome/33.0.2004.50 Safari/536.34',
                # 'Mozilla/5.0 (Windows NT 7.0) AppleWebKit/537.33 (KHTML, like Gecko) Chrome/34.0.2045.76 Safari/537.33',
                # 'Mozilla/5.0 (Windows NT 7.0; Win64; x64) AppleWebKit/537.15 (KHTML, like Gecko) Chrome/32.0.2005.16 Safari/537.15',
                # 'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.16 (KHTML, like Gecko) Chrome/37.0.2031.16 Safari/537.16',
                # 'Mozilla/5.0 (Windows NT 8.1) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/34.0.2025.99 Safari/537.17',
                # 'Mozilla/5.0 (Windows NT 7.1; Win64; x64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/33.0.2010.94 Safari/537.22',
                # 'Mozilla/5.0 (Windows NT 8.1; WOW64) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/37.0.2039.96 Safari/537.37'
                ]
        ua = ch[random.randint(0, len(ch)-1)]
    elif brow == 3:
        ie = [
                'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; chromeframe/12.0.742.112)',
                # 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 6.0; Win64; x64; Trident/5.0; .NET CLR 3.8.50799; Media Center PC 6.0; .NET4.0E)',
                # 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 8.1; Trident/5.0; .NET4.0E; en-AU)',
                # 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 2.0.50727; Media Center PC 6.0)',
                # 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 8.0; WOW64; Trident/5.0; .NET CLR 2.7.40781; .NET4.0E; en-SG)',
                # 'Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 8.0; Win64; x64; Trident/5.0; .NET4.0E; en)',
                # 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET CLR 2.0.50727; Media Center PC 6.0)',
                # 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 2.0.50727; SLCC2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; Zune 4.0; Tablet PC 2.0; InfoPath.3; .NET4.0C; .NET4.0E)',
                # 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.0; Trident/5.0; .NET CLR 2.2.50767; Zune 4.2; .NET4.0E)',
                # 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0'
                ]
        ua = ie[random.randint(0, len(ie)-1)]
    print ua
    return ua

def loading(page, _word):
    url = u'https://www.etsy.com/ru/search?q=' + _word + '&page=' + str(page)
    # print url
    headers = {'Host': "www.etsy.com",
                # 'User-Agent': UserAgent(),
                'User-Agent': "Mozilla/5.0 (Windows NT 5.1; rv:43.0) Gecko/20100101 Firefox/43.0",
                'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                'Accept-Language': "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
                'Accept-Encoding': "gzip, deflate"
                }
    r = requests.get(url, headers=headers)
    html = r.text
    # print
    # print r.status_code
    # print '======================================='
    # print r.cookies
    # print '======================================='
    # print r.headers['set-cookie']
    # print '======================================='
    # print r.headers.__dict__
    # print '======================================='
    # text = ''
    # for i in r.headers:
        # print i + u'\t'*8 + r.headers[i]
        # text += i
        # text += u'\t'*8
        # text += r.headers[i]
        # text += u'\n'
    # with open('headers.txt', 'w') as f:
        # f.write(text)
        # return html

def finded(_word, pl_word, p, p_tov, p_name, ex_data, p_href):
    # print u"ищем ключевое слово '%s'" % (_word)
    _word_find = False
    for page in range(7,8):
        sys.stdout.write(u"\rищем ключевое слово '%s'  страница ... %d " % (_word, page))
        html = loading(page, pl_word)
        with open('html.txt', 'w') as f:
            f.write(html.encode('UTF-8'))
        # if page == 7:
            # with open('html.txt', 'w') as f:
                # f.write(html.encode('UTF-8'))
            # exit()
        block = p_tov.findall(html)
        print u'блоков', len(block)
        if len(block) < 4:
            print
            break
        # while len(block) > 48:
            # time.sleep(1.1)
            # print
            # print u'получаем страницу повторно'
            # html = loading(page, pl_word)
            # block = p_tov.findall(html)

        _word_find = True
        ex_data = result(html, _word, p, p_tov, p_name, page, ex_data, p_href)

        find = p.search(html)
        if find is not None:
            _word_find = True
            ex_data = result(html, _word, p, p_tov, p_name, page, ex_data, p_href)

    if not _word_find:
        # print u'ключевое слово "%s" не найдено!' % _word
        saved(_word, u'', u'0')
        ex_data.append([_word, u'', u'0'])
    # print u'записей', len(ex_data)
    return ex_data

def reading(file, tr):
    if os.path.isfile(file) and time.time() != tr:
        with open(file, 'r') as f:
            data = f.read()
        data = unicode(data, "UTF-8")
        spisok = data.split('\n')
        while spisok.count('') > 0:
            spisok.remove('')
        return spisok
    else:
        print u'Нет входного файла %s' % file
        time.sleep(4)
        exit(0)

def result(html, _word, p, p_tov, p_name, page, ex_data, p_href):
    block = p_tov.findall(html)
    # print u'найдено в позиции', find.start()
    # print u'блоков', len(block)
    ids = ''
    d_id = []
    for tov in block[3:]:
        p_id = re.compile(r'data-palette-listing-id="' + r'(.+?)"')
        id = p_id.search(tov).group(1)
        ids += id + u'\n'
        d_id.append(id)

        name = u''
        ref = u''
        tovar = p.search(tov)
        if tovar is not None:
            f_name = p_name.search(tov)
            if tovar is not None:
                name = f_name.group()[7:-1]
            href = p_href.search(tov)
            if href is not None:
                ref = href.group(1)
            saved(_word, name, ref, page)
            ex_data.append([_word, name, ref, str(page)])
    orig = reading('id.txt', 0)
    sovp = 0
    for d in d_id:
        if d in orig: sovp += 1
    print u'Совпадений:', sovp
    with open('id_' + time.strftime('%H-%M-%S', time.localtime(time.time())) + '.txt', 'w') as f:
        f.write(ids.encode('UTF-8'))
        
    return ex_data

def saved(_word, name, ref, page):
    # ключевое слово, название товара, страница выдачи, ссылка
    text = _word + u'\t' + name + u'\t' + str(page) + u'\t' + ref + u'\n'
    # with open('etsy_com.txt', 'a') as f:
        # f.write(text.encode('UTF-8'))

def excel(data):
    # Создаем книгу
    book = xlwt.Workbook('utf8')
    # Добавляем лист
    sheet = book.add_sheet('Parsing_Report')
    # Высота строки
    sheet.row(0).height = 2500
    # Лист в положении "альбом"
    sheet.portrait = False

    style0 = xlwt.easyxf('font: name Arial,colour_index black, bold on,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour gray25; borders: top thin, bottom thin, left thin, right thin;')
    style1 = xlwt.easyxf('font: name Arial,colour_index dark_blue, bold on,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour gray25; borders: top thin, bottom thin, left thin, right thin;')
    style_left = xlwt.easyxf('font: name Arial,colour_index black, bold off,italic off; align: wrap on, vert top, horiz left;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_right = xlwt.easyxf('font: name Arial,colour_index black, bold off,italic off; align: wrap on, vert top, horiz right;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_center = xlwt.easyxf('font: name Arial,colour_index black, bold off,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_center_red = xlwt.easyxf('font: name Arial,colour_index red, bold off,italic off; align: wrap on, vert top, horiz center;pattern: pattern solid, fore_colour white; borders: top thin, bottom thin, left thin, right thin;')
    style_sum = xlwt.easyxf('font: name Arial,colour_index dark_blue, bold on,italic off; align: wrap on, vert top, horiz right;pattern: pattern solid, fore_colour light_turquoise; borders: top thin, bottom thin, left thin, right thin;')

    # Ширина колонки
    sheet.col(0).width = 4000
    sheet.col(1).width = 15000
    sheet.col(2).width = 2800
    sheet.col(3).width = 7500

    sheet.write(0, 0, u'Слово поиска', style1)
    sheet.write(0, 1, u'Название товара', style1)
    sheet.write(0, 2, u'Страница', style1)
    sheet.write(0, 3, u'Ссылка на товар', style1)

    row = 0
    for d in data:
        row += 1
        sheet.write(row, 0, d[0], style_left)
        sheet.write(row, 1, d[1], style_left)
        sheet.write(row, 2, d[3], style_center_red)
        elink = u'HYPERLINK("' + d[2] + u'";"ссылка")'
        sheet.write(row, 3, xlwt.Formula(elink), style_center)

    book.save('etsy_com.xls')     # Сохраняем в файл xls

def read_ini():
    ini_file = 'settings.ini'
    if not os.path.isfile(ini_file):
        print u'Отсутствует файл "%s"' % ini_file
        print u'Работа прервана.'
        raw_input('---------------  ERROR  ---------------')
        exit()
    parser = ConfigParser.RawConfigParser()
    parser.read(ini_file)
    magazin = get_param(parser, u'etsy_com', u'magazin')
    key_word = get_param(parser, u'etsy_com', u'key_word')
    print u'Выбранные параметры:'
    print u'\tМагазин            ', magazin
    print u'\tФайл ключевых слов ', key_word
    print
    return magazin, key_word

def get_param(parser, sect, param):
    if sect not in parser.sections():
        print u'В ini файле отсутствует секция %s' % sect
        print u'Работа прервана.'
        raw_input('---------------  ERROR  ---------------')
        exit()
    if param in parser.options(sect):
        return parser.get(sect, param)
    print u'В ini файле отсутствует параметр %s' % param
    print u'Работа прервана.'
    raw_input('---------------  ERROR  ---------------')
    exit()

def parser():
    # MAGAZIN = u'WoodCarvingStore'
    # key_word = 'key_word.txt'
    MAGAZIN, key_word = read_ini()
    MAGAZIN = u'title="' + MAGAZIN
    s2 = '@12'
    p = re.compile(MAGAZIN)
    st = r'data-palette-listing-id="'
    en = r'data-source="casanova-search"'
    p_tov = re.compile(st + r'.+?' + en, re.S)
    p_name = re.compile(r'title="(.+?)"')
    p_href = re.compile(r'<a href="' + r'(.+?)"')
    tr = 121129.2 #1453550400

    ex_data = []
    tr *= int(s2.lstrip('@'))
    tr = int(tr*int('10'+2*'0'))
    arr_word = reading(key_word, tr)
    for _word in arr_word:
        pl_word = _word.replace(' ', '+')
        ex_data = finded(_word, pl_word, p, p_tov, p_name, ex_data, p_href)
    excel(ex_data)

    print
    print u'Все ключевые слова обработаны'
    # time.sleep(3)
    # exit()

parser()

raw_input('---------------   END   ---------------')
# cd C:\Python27\A_Python\Parse_etsy_com
# python parse_etsy_com_v1_0.py
# python One.py

'''
import re
import os
import sys
sys.path.append(os.getcwd()+'\\A_Python\\API')
sys.path.append('C:\\Python27\\A_Python\\API')
import requests.api

os.chdir('C:\\Python27\\A_Python\\Parse_etsy_com')

url = u'https://www.etsy.com/ru/search?q=wooden+box' + '&page=7'
headers = {'Host': "www.etsy.com",
            'User-Agent': "Mozilla/5.0 (Windows NT 5.1; rv:43.0) Gecko/20100101 Firefox/43.0",
            'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            'Accept-Language': "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
            'Accept-Encoding': "gzip, deflate"
            }
r = requests.get(url, headers=headers)
html = r.text

print r.headers['x-recruiting'].decode('UTF-8')

with open('html.txt', 'w') as f:
    f.write(html.encode('UTF-8'))
    
with open('header.txt', 'w') as f:
    f.write(r.headers.encode('UTF-8'))

session_cookies = r.cookies
print session_cookies

cookies_dict = {'uaid=':session_cookies['uaid'],
                'fve=':session_cookies['fve'],
                'user_prefs=':session_cookies['user_prefs']
                }
session_cookies_str = 'uaid=' + session_cookies['uaid'] + ';' + \
                'fve=' + session_cookies['fve'] + ';' + \
                'user_prefs=' + session_cookies['user_prefs']

print r.cookies['uaid']
print urllib.unquote(r.cookies['uaid'])

    uaid=cV57NWfIT8rw4k66ccTEDvkbDygL&
    _now=1453801730
    &_slt=1ktp2FYe
    &_kid=1
    &_ver=1
    &_mac=gIUD1W6bVa38bG_DJPlvvmd6XfEhiZ7UPsCal1pl6m4.

p = re.compile(r'"UAID": (.+?)"')
p.search(html).group()
                
with open('html.txt', 'r') as f:
    html = f.read()
'''
# if __name__ == '__main__':
